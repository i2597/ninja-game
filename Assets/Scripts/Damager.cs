using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Damager : MonoBehaviour
{
    [SerializeField] private float damageAmount;
    [SerializeField] private string tag;

    private Dictionary<int, IDamagable> damageableDict = new Dictionary<int, IDamagable>();


    public void OnTriggerEnter(Collider other)
    {
       
        IDamagable damagable = other.GetComponent<IDamagable>();

        if (other.tag == tag && damagable != null && !damageableDict.ContainsKey(damagable.GetUniqueIdentifier()))
        {
            
            damageableDict[damagable.GetUniqueIdentifier()] = damagable;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        IDamagable damagable = other.GetComponent<IDamagable>();
        Debug.Log("Being called");
        if (other.tag == tag && damagable != null && damageableDict.ContainsKey(damagable.GetUniqueIdentifier()))
        {
            Debug.Log("Removing " + damagable.GetUniqueIdentifier());
            damageableDict.Remove(damagable.GetUniqueIdentifier());
        }
    }

    public void DamageObjectsInRange()
    {
        Debug.Log(damageableDict.Count);
        foreach (IDamagable damagable in damageableDict.Values)
        {
            damagable.OnDamage(damageAmount);
        }
    }
}
