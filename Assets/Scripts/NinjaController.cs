using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaController : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private float turnSpeed;


    [SerializeField] private Rigidbody rb;
    [SerializeField] private Animator anim;

    [SerializeField] private float groundCheckInterval;
    [SerializeField] private float groundingDistance;


    [SerializeField] private KeyCode jumpKey;
    [SerializeField] private KeyCode attackKey;
    [SerializeField] private float jumpForce;

    [SerializeField] private Damager damager;

    private bool shouldJump;
    

    private bool grounded = false;

    private float verticalAxisInput;
    private float horizontalAxisInput;

    private Vector3 movementVector;
    private WaitForSeconds groundCheckWait;

    private void Awake()
    {
        groundCheckWait = new WaitForSeconds(groundCheckInterval);
        StartCoroutine(GroundCheckRoutine());
    }

    private IEnumerator GroundCheckRoutine()
    {
        while(true)
        {
            grounded = Physics.Raycast(transform.position, -transform.up, groundingDistance);
            anim.SetBool("grounded", grounded);
            yield return groundCheckWait;
        }
    }

    // Update is called once per frame
    void Update()
    {
        verticalAxisInput = Input.GetAxis("Vertical");
        horizontalAxisInput = Input.GetAxis("Horizontal");

        anim.SetFloat("forwardVelocity", verticalAxisInput);
        anim.SetFloat("verticalVelocity", rb.velocity.y);

        if(Input.GetKeyDown(attackKey))
        {
            anim.SetTrigger("attack");
        }

        if(grounded && Input.GetKeyDown(jumpKey))
        {
            anim.SetTrigger("jump");
            shouldJump = true;
        }
        
    }

    private void FixedUpdate()
    {
        if (shouldJump)
        {
            rb.AddForce(Vector3.up * jumpForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
            shouldJump = false;
        }

        if (Mathf.Abs(verticalAxisInput) > 0.1f)
        {
            movementVector = transform.forward * verticalAxisInput * movementSpeed * Time.fixedDeltaTime;
        }
        else
        {
            movementVector = Vector3.zero;
        }

        rb.velocity = new Vector3(movementVector.x, rb.velocity.y, movementVector.z);

        if (Mathf.Abs(horizontalAxisInput) > 0.1f)
        {
            rb.angularVelocity = transform.up * horizontalAxisInput * turnSpeed * Time.fixedDeltaTime;
        }
        else
        {
            rb.angularVelocity = Vector3.zero;
        } 
    }

    private void OnTriggerEnter(Collider other)
    {
        damager.OnTriggerEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        damager.OnTriggerExit(other);
    }


}
