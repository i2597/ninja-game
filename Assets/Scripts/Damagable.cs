using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damagable : MonoBehaviour, IDamagable
{
    [SerializeField] private UnityEvent<float> onDamage;

    private bool isActive = true;

    public int GetUniqueIdentifier()
    {
        return gameObject.GetInstanceID();
    }

    public bool IsActive()
    {
        return isActive;
    }

    public void OnDamage(float damage)
    {
        if(isActive)
        {
            onDamage?.Invoke(damage);
        }
        isActive = false;
        
    }
}
