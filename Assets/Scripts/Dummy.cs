using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy : MonoBehaviour
{
    [SerializeField] private bool partOfGame;
    [SerializeField] private Animator anim;
    

    public void HitDummy()
    {
        if (partOfGame)
        {
            DummyGame.Instance.ReportHit();
        }
        anim.SetTrigger("Struck");
    }
}
