using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable 
{
    public bool IsActive();
    public void OnDamage(float damage);

    public int GetUniqueIdentifier();
}
