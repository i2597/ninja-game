using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class DummyGame : MonoBehaviour
{

    [SerializeField] private int seconds;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private int numDummies;

    [SerializeField] private UnityEvent countdownCompleteSuccessful;
    [SerializeField] private UnityEvent countdownCompleteUnsuccessful;

    private bool timerActive;
    private bool won = false;

    public static DummyGame Instance;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(gameObject);
        }
        Instance = this;

        StartCoroutine(TimerRoutine());
    }

    public void ReportHit()
    {
        numDummies--;

        if(numDummies <= 0)
        {
            won = true;
        }
    }

    private IEnumerator TimerRoutine()
    {
        
        int countdownTime = seconds;

        while (!won && countdownTime > 0)
        {
            timerText.text = countdownTime.ToString();
            yield return new WaitForSeconds(1);
            countdownTime--;
        }

        if(won)
        {
            countdownCompleteSuccessful?.Invoke();
        }
        else
        {
            countdownCompleteUnsuccessful?.Invoke();
        }
    }
}
